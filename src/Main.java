public class Main {

//    Komentarze zaczyna sie od dwoch ukosnikow // lub /* jesli bedzie wiecje niz jedna lina

/*  W tej lini mowimy Javie dwie rzeczy.
1.Chcemy zdefiniowac zmienna halloUK ktora bedzie przechowywac dane typu String.
2, Przypisujemy jej wartosc  Hallo UK.
Karzda pelne samodzielne wyrazenie musi sie konczyc ; Narazie ignoruj public i static.
 */
    static String halloUK = "Hallo UK!";
//    Definiujemy cos w rodzaju listy ktora zawierac bedzie kraje
    enum Kraje { AU, PL, US, FR, IT };

/* Tutaj ponownie dokoujemy definicji ale tym razem jest to metoda(funkcjia).
 W tej metodzie ze wzgledu na uzyte void deklarujemy sie ze nie bedziemy zwracac nic z niej.
 Puste nawiasy mowio Javie ze metoda ta nie bierze rowniez zadnych parametrow.
 */
    static void dwa(){
        // System.out.println jest wewnetrzna metoda Javy uzywana do drkukowania pojedynczej lini
        System.out.println(halloUK);

//      Wykonujemy pentle po elementach w Kraje i uzywamy ich do stworzenia tekstu koncowego z system.out...
//        Znak + jest uzywany do waczenia elementow tego samego rodzaju w naszym przypadku tekstu

        for (Kraje k : Kraje.values()){
            System.out.println("Hallo " + k + "!");
        }
    }

//     Definicja metody trzy ktora zwraca za pomoca 'return' dage rodzaju String. To co jest zwrocone bedzie przekazane do przetwarzania metodzie ktora wykonala metode trzy.
//  Metoda ta rowniez bierze do poprawnego dzialania parametr 'slowo' typu String to co jest w nawiasie
     static String trzy(String slowo){
         return slowo + " Hello World!";
     }

//    Metoda main jest metoda ktora jest wykonywana jako pierwsza gdy program jest uruchamiany tzw wejscie do programu
    public static void main(String[] args) {
// Podobnie jak powyzej chcemy wydrukowac tekst za pomoca metody system.out...
        System.out.println("Hello World!");
//       Wykonujemy wczesniej skonstruowana metode dwa
        dwa();
//        Drukujemy to co nam zwroci nasza metoda trzy jak jej damy jako argument zmienna halloUK
        System.out.println(trzy(halloUK));

    }
}
/* Po uruchomienu powinienes zobatrzec cos takiego:
Hello World!
Hallo UK!
Hallo UK!
Hallo PL!
Hallo US!
Hallo FR!
Hallo IT!
Hallo UK! Hello World!
*/
